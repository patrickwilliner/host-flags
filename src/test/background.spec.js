import { isNotBlank, isString, parseHostFromUrl, createServiceLimiter } from '../api.js';

describe('#isString', () => {
  test('with non string values should be falsy', () => {
    expect(isString(7)).toBe(false);
    expect(isString(true)).toBe(false);
    expect(isString(false)).toBe(false);
    expect(isString({})).toBe(false);
    expect(isString(new Date())).toBe(false);
    expect(isString(NaN)).toBe(false);
    expect(isString(undefined)).toBe(false);
    expect(isString(null)).toBe(false);
  });

  test('with string values should be truthy', () => {
    expect(isString('')).toBe(true);
    expect(isString(' ')).toBe(true);
    expect(isString('7')).toBe(true);
  });
});

describe('#isNotBlank', () => {
  test('with any kind of nil argument should be falsy', () => {
    expect(isNotBlank(null)).toBe(false);
    expect(isNotBlank(undefined)).toBe(false);
    expect(isNotBlank()).toBe(false);
  });

  test('with empty string should be falsy', () => {
    expect(isNotBlank('')).toBe(false);
  });

  test('with non string argument should be falsy', () => {
    expect(isNotBlank(0)).toBe(false);
    expect(isNotBlank(5)).toBe(false);
    expect(isNotBlank(NaN)).toBe(false);
    expect(isNotBlank(true)).toBe(false);
    expect(isNotBlank(false)).toBe(false);
    expect(isNotBlank({})).toBe(false);
  });

  test('with string argument should be truthy', () => {
    expect(isNotBlank('jump')).toBe(true);
    expect(isNotBlank(' ')).toBe(true);
  });

  test('with trimed empty string and option "trim=true" should be falsy', () => {
    expect(isNotBlank(' ', { trim: true })).toBe(false);
  });
});

describe('#parseHostFromUrl', () => {
  test('with unsupported protocol should be ""', () => {
    expect(parseHostFromUrl('chrome://about')).toBe('');
    expect(parseHostFromUrl('opera://about')).toBe('');
    expect(parseHostFromUrl('vivaldi://about')).toBe('');
    expect(parseHostFromUrl('smb://192.168.11.12')).toBe('');
    expect(parseHostFromUrl('afs://10.0.0.1')).toBe('');
    expect(parseHostFromUrl('ssh://10.44.0.1')).toBe('');
    expect(parseHostFromUrl('svn://192.168.11.11')).toBe('');
  });

  test('localhost should be ""', () => {
    expect(parseHostFromUrl('localhost')).toBe('');
    expect(parseHostFromUrl('127.0.0.1')).toBe('');
    expect(parseHostFromUrl('http://localhost')).toBe('');
    expect(parseHostFromUrl('http://127.0.0.1')).toBe('');
    expect(parseHostFromUrl('https://localhost')).toBe('');
    expect(parseHostFromUrl('https://127.0.0.1')).toBe('');
    expect(parseHostFromUrl('ftp://localhost')).toBe('');
    expect(parseHostFromUrl('ftp://127.0.0.1')).toBe('');
    expect(parseHostFromUrl('http://localhost:4200')).toBe('');
    expect(parseHostFromUrl('http://localhost:4200/books')).toBe('');
    expect(parseHostFromUrl('http://localhost:4200/books?id=321')).toBe('');
    expect(parseHostFromUrl('http://localhost:4200/books?id=321')).toBe('');
  });

  test('invalid urls should be ""', () => {
    expect(parseHostFromUrl('what is the capital of California?')).toBe('');
    expect(parseHostFromUrl(' ')).toBe('');
    expect(parseHostFromUrl('./tata.ta')).toBe('');
    expect(parseHostFromUrl('/etc/hosts')).toBe('');
  });
});

describe('serviceLimiter', () => {
  test('should return true when limited and false when not', done => {
    const thresholdCalls = 5;
    const perNumberOfSeconds = 1;
    let serviceLimiter = createServiceLimiter(thresholdCalls, perNumberOfSeconds);

    for (let i = 0; i < thresholdCalls; i++) {
      expect(serviceLimiter()).toBe(false);
    }

    expect(serviceLimiter()).toBe(true);

    setTimeout(() => {
      expect(serviceLimiter()).toBe(false);
      done();
    }, perNumberOfSeconds * 1000);
  });
});