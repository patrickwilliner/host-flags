import * as fs from 'fs';

const FOLDERS_TO_REMOVE = [
  'dist'
];

function deleteFolderRecursive(path) {
  if (fs.existsSync(path) && fs.lstatSync(path).isDirectory()) {
    fs.readdirSync(path).forEach(file => {
      let curPath = `${path}/${file}`;

      if (fs.lstatSync(curPath).isDirectory()) {
        deleteFolderRecursive(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });

    console.log(`Deleting directory "${path}"...`);
    fs.rmdirSync(path);
  }
};

function run() {
  console.log('Cleaning working tree...');

  FOLDERS_TO_REMOVE.forEach(folderPath => {
    deleteFolderRecursive(folderPath);
  });
  console.log('Clean successful.');
}

run();