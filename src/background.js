'use strict';

import { handleTabUrlModification, handleTabSelectionChanged, handleTabClosed } from './api';

const COMPLETE = 'complete';

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  if (changeInfo != null && changeInfo.url != null) {
    handleTabUrlModification(changeInfo.url, tabId);
  }
  if (changeInfo != null && changeInfo.status === COMPLETE) {
    handleTabUrlModification(tab.url, tabId);
  }
});

chrome.tabs.onSelectionChanged.addListener(tabId => {
  handleTabSelectionChanged(tabId);
});

chrome.tabs.onRemoved.addListener(tabId => {
  handleTabClosed(tabId);
});