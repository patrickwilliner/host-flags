'use strict';

// TODO use https
const URL_DOMAIN_TO_COUNTRY_SERVICE = 'http://ip-api.com/json';
const HTTP_OK = 200;
const SUPPORTED_URL_PROTOCOLS = ['http:', 'https:', 'ftp:'];
const UNSUPPORTED_HOSTS = ['localhost', '127.0.0.1'];

let hosts = {};
let serviceLimitReached = createServiceLimiter(45, 60); // limits service to 45 calls per 60 seconds

export function handleTabUrlModification(newUrlString, tabId) {
  let newUrl = parseUrl(newUrlString);

  if (isValidUrl(newUrl)) {
    lookupHostGeoLocation(newUrl).then(geoLocation => {
      cacheGeoLocation(geoLocation, tabId);
      updateFlag(tabId);
    }).catch(reason => {
      console.error(reason);
    });
  }
}

export function updateFlag(tabId) {
  setIcon(tabId);
  setTitle(tabId);
}

export function handleTabClosed(tabId) {
  delete hosts[tabId];
}

export function handleTabSelectionChanged(tabId) {
  updateFlag(tabId);
}

export function parseUrl(urlString) {
  return new URL(urlString);
}

export function isValidUrl(url) {
  return urlHasSupportedProtocol(url) && urlHasSupportedHost(url);
}

export function cacheGeoLocation(data, tabId) {
  hosts[tabId] = {
    countryCode: data.countryCode.toLowerCase(),
    countryName: data.country,
    city: data.city,
    timestamp: new Date().getTime()
  }
}

export function setIcon(tabId) {
  if (hosts[tabId] != null && hosts[tabId].countryCode != null) {
    chrome.pageAction.setIcon({ tabId: tabId, path: 'icon/' + hosts[tabId].countryCode + '.png' });
    chrome.pageAction.show(tabId);
  }
}

export function setTitle(tabId) {
  if (hosts[tabId] != null && hosts[tabId].countryName != null) {
    chrome.pageAction.setTitle({ title: hosts[tabId].countryName, tabId: tabId });
  }
}

export function lookupHostGeoLocation(url) {
  if (!serviceLimitReached()) {
    return resolveHostCountry(url.hostname);
  } else {
    return createRejectedPromise('can\'t lookup url: ' + url);
  }
}

export function urlHasSupportedProtocol(url) {
  return url != null && SUPPORTED_URL_PROTOCOLS.indexOf(url.protocol) >= 0;
}

export function urlHasSupportedHost(url) {
  return isNotBlank(url.hostname) && UNSUPPORTED_HOSTS.indexOf(url.hostname) < 0;
}

export function serviceBlocked() {
  return (new Date().getTime() - serviceBlockedTill) < 0;
}

export function resolveHostCountry(url) {
  return fetch(`${URL_DOMAIN_TO_COUNTRY_SERVICE}/${encodeURIComponent(url)}`)
    .then(response => response.json());
}

export function isString(value) {
  return value != null && (typeof value === 'string' || value instanceof String);
}

export function isNotBlank(value, options) {
  return isString(value) && (options != null && options.trim ? value.trim() : value).length > 0;
}

export function parseHostFromUrl(url) {
  if (isNotBlank(url, { trim: true })) {
    try {
      let urlObj = new URL(url);
      if (urlHasSupportedProtocol(urlObj) && urlHasSupportedHost(urlObj)) {
        return urlObj.hostname;
      }
    } catch (ignored) {
    }
  }
  return '';
}

export function createRejectedPromise(reason) {
  return new Promise((resolve, reject) => {
    reject(reason);
  });
}

export function createServiceLimiter(callThreshold, perNumberOfSeconds) {
  let slots = [];

  return function serviceLimiter() {
    for (let i = slots.length - 1; i >= 0; i--) {
      if ((new Date().getTime() - slots[i]) > (perNumberOfSeconds * 1000)) {
        slots.splice(i, 1);
      }
    }
    slots.push(new Date().getTime());
    return slots.length > callThreshold;
  }
}